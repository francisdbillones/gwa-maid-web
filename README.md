# gwa-maid-web

GWA Calc on steroids.

# Running

Opening the HTML files will not work. As the scripts are modules, you will need to run a local webserver using a lightweight server like the `http-server` npm package.

If you do not want to serve the files yourself the website is available on [GitHub Pages.](https://francisdbillones.github.io/gwa-maid-web)
