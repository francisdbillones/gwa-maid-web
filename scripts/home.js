import * as api from './api_interface.js';
import * as cache from './cache.js';
import * as helpers from './helpers.js';
// check for localStorage support
if (!helpers.verifyLocalStorage()) {
    alert('We\'re sorry, but your browser does not support local storage. Our site will not work for you.');
    window.close();
}
const token = localStorage.getItem('token');
let subjects;
document.addEventListener('DOMContentLoaded', async () => {
    // wait for response from server. when response is received,
    // clear the loading indicator and display content
    const main = document.getElementById('container');
    const main_loading_indicator = document.getElementById('main-loading-indicator');
    await helpers.redirectIfNotAuthenticated(token);
    await loadVariables();
    main_loading_indicator.classList.add('d-none');
    main.classList.remove('d-none');
    const sign_out_button = document.getElementById('sign-out-button');
    sign_out_button.onclick = () => {
        helpers.signOut();
        window.location.replace(helpers.BASE_URL);
    };
    const predicted_grade_label = document.getElementById('predicted-grade-label');
    const table = document.querySelector('table');
    const reload = document.getElementById('reload');
    const form = document.querySelector('form');
    const subject_name_field = form.elements.namedItem('subject-name');
    const subject_weight_field = form.elements.namedItem('subject-weight');
    subject_name_field.oninput = () => {
        subject_name_field.setCustomValidity('');
    };
    subject_weight_field.oninput = () => {
        subject_weight_field.setCustomValidity('');
    };
    if (window.navigator.onLine) {
        api.getSubjects(token).then(remote_subjects => {
            cache.saveSubjects(remote_subjects);
            helpers.writeSubjectsToTable(table, remote_subjects);
        });
    }
    if (subjects != null) {
        helpers.writeSubjectsToTable(table, subjects);
        let predicted_grade_in_gwa = helpers.computePredictedGWAFromSubjects(subjects);
        predicted_grade_label.innerText = `Predicted grade: ${predicted_grade_in_gwa.toPrecision(4)}`;
    }
    reload.onclick = (e) => {
        e.preventDefault();
        const loading_indicator = reload.querySelector('span');
        loading_indicator.classList.remove('d-none');
        const reload_icon = reload.querySelector('img');
        reload_icon.classList.add('d-none');
        loadVariables().then(() => {
            loading_indicator.classList.add('d-none');
            reload_icon.classList.remove('d-none');
        });
    };
    form.onsubmit = (e) => {
        e.preventDefault();
        const submit_button = document.getElementById('submit-button');
        // check validity for each input field
        document.querySelectorAll('input').forEach((field) => {
            if (!field.checkValidity()) {
                field.reportValidity();
                submit_button.removeAttribute('disabled');
                document.getElementById('submit-button-loading-indicator').classList.add('d-none');
                return;
            }
        });
        let subject_name = subject_name_field.value;
        // on submit, add loading indicator to button
        // and make it disabled
        submit_button.setAttribute('disabled', '');
        submit_button.querySelector('span').classList.remove('d-none');
        // check that the subject name doesn't already exist
        if (subjects.map(subject => subject.name).includes(subject_name)) {
            subject_name_field.setCustomValidity('That name is taken.');
            subject_name_field.reportValidity();
            submit_button.removeAttribute('disabled');
            document.getElementById('submit-button-loading-indicator').classList.add('d-none');
            return;
        }
        else {
            subject_name_field.setCustomValidity('');
        }
        let subject_weight = parseFloat(subject_weight_field.value);
        subject_weight_field.setCustomValidity('');
        let sum_of_subject_weights = subjects.map(subject => subject.weight).reduce((sum, weight) => sum + weight, 0);
        // check that the total weight won't go over 100%
        if ((sum_of_subject_weights + subject_weight) / 100 > 1) {
            let total_weight = (sum_of_subject_weights + subject_weight);
            let total_weight_in_percent = total_weight.toPrecision(4) + '%';
            subject_weight_field.setCustomValidity(`Adding this weight would result in a cumulative weight of ${total_weight_in_percent}.`);
            submit_button.removeAttribute('disabled');
            document.getElementById('submit-button-loading-indicator').classList.add('d-none');
            subject_weight_field.reportValidity();
            return;
        }
        else {
            subject_weight_field.setCustomValidity('');
        }
        let subject = {
            name: subject_name,
            weight: subject_weight,
            last_updated: new Date().toLocaleString('en-US'),
            predicted_grade: api.DEFAULT_GRADE
        };
        // save changes to localStorage
        cache.addSubject(subject);
        // if online, save changes to server
        if (window.navigator.onLine) {
            api.addSubject(token, subject).then(response => {
                if (response) {
                    window.location.reload();
                }
                else {
                    submit_button.removeAttribute('disabled');
                    document.getElementById('submit-button-loading-indicator').classList.add('d-none');
                    alert('Something went wrong.');
                }
            });
        }
    };
});
async function loadVariables() {
    if (window.navigator.onLine) {
        subjects = await api.getSubjects(token);
        cache.saveSubjects(subjects);
    }
    else {
        subjects = cache.getSubjects();
    }
}
// check every 5 seconds that user is online
// if user is offline, notify them
let internetChecker = setInterval(() => {
    if (!window.navigator.onLine) {
        alert('You are currently offline. Any changes you make will not be saved to our servers.');
        clearInterval(internetChecker);
    }
}, 5000);
