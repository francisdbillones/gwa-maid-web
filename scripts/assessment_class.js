import * as api from './api_interface.js';
import * as helpers from './helpers.js';
import * as cache from './cache.js';
// check for localStorage support
if (!helpers.verifyLocalStorage()) {
    alert('We\'re sorry, but your browser does not support local storage. Our site will not work for you.');
    window.close();
}
// if user has an existing token, verify it
// if it's valid, redirect to home.html
const token = localStorage.getItem('token');
// get parameters from arguments in URL
let url = new URL(window.location.href);
const subject_name = url.searchParams.get('subject');
const assessment_class_name = url.searchParams.get('assessment_class');
let subjects;
let subject;
let assessment_class;
document.addEventListener('DOMContentLoaded', async () => {
    const main = document.getElementById('container');
    const main_loading_indicator = document.getElementById('main-loading-indicator');
    await helpers.redirectIfNotAuthenticated(token);
    await loadVariables();
    // hide loading indicator and display content
    main_loading_indicator.classList.add('d-none');
    main.classList.remove('d-none');
    const back_button = document.getElementById('back-button');
    back_button.onclick = () => {
        window.location.replace(`${helpers.BASE_URL}/htdocs/subject.html?` + new URLSearchParams({
            subject: subject.name,
        }));
    };
    const sign_out_button = document.getElementById('sign-out-button');
    sign_out_button.onclick = () => {
        helpers.signOut();
        window.location.replace(helpers.BASE_URL);
    };
    const predicted_grade_label = document.getElementById('predicted-grade-label');
    predicted_grade_label.innerText = `Your predicted grade for ${assessment_class.name} in ${subject.name}: ${assessment_class.predicted_grade.toPrecision(4)}%`;
    const table = document.querySelector('table');
    const reload = document.getElementById('reload');
    const form = document.querySelector('form');
    const assessment_name_field = form.elements.namedItem('assessment-name');
    const assessment_grade_field = form.elements.namedItem('assessment-grade');
    assessment_name_field.oninput = () => {
        assessment_name_field.setCustomValidity('');
    };
    assessment_grade_field.oninput = () => {
        assessment_grade_field.setCustomValidity('');
    };
    if (assessment_class.assessments != null) {
        helpers.writeAssessmentsToTable(table, subject, assessment_class);
    }
    reload.onclick = async (e) => {
        e.preventDefault();
        const loading_indicator = reload.querySelector('span');
        loading_indicator.classList.remove('d-none');
        const reload_icon = reload.querySelector('img');
        reload_icon.classList.add('d-none');
        loadVariables().then(() => {
            loading_indicator.classList.add('d-none');
            reload_icon.classList.remove('d-none');
        });
    };
    form.onsubmit = (e) => {
        e.preventDefault();
        // check validity for each input field
        document.querySelectorAll('input').forEach((field) => {
            if (!field.checkValidity()) {
                field.reportValidity();
            }
        });
        let assessment_name = assessment_name_field.value;
        // on submit, add loading indicator to button
        // and make it disabled
        const submit_button = document.getElementById('submit-button');
        submit_button.setAttribute('disabled', '');
        document.getElementById('submit-button-loading-indicator').classList.remove('d-none');
        // check that the name doesn't already exist
        if (assessment_class.assessments.
            map(assessment => assessment.name).
            includes(assessment_name)) {
            assessment_name_field.setCustomValidity('That name is taken.');
            assessment_name_field.reportValidity();
            submit_button.removeAttribute('disabled');
            document.getElementById('submit-button-loading-indicator').classList.add('d-none');
            return;
        }
        else {
            assessment_name_field.setCustomValidity('');
        }
        let assessment_grade = parseFloat(assessment_grade_field.value);
        let assessment = {
            name: assessment_name,
            assessment_class: assessment_class,
            last_updated: new Date().toLocaleString('en-US'),
            grade: assessment_grade,
        };
        // save changes to localStorage
        cache.addAssessment(subject, assessment);
        // if online, save changes to server
        if (window.navigator.onLine) {
            api.addAssessment(token, subject, assessment).then((response) => {
                if (response) {
                    window.location.reload();
                }
                else {
                    submit_button.removeAttribute('disabled');
                    document.getElementById('submit-button-loading-indicator').classList.add('d-none');
                    alert('Something went wrong when uploading data.');
                }
            });
        }
    };
});
async function loadVariables() {
    if (window.navigator.onLine) {
        subjects = await api.getSubjects(token);
        cache.saveSubjects(subjects);
    }
    else {
        subjects = cache.getSubjects();
    }
    if (subject_name == null) {
        alert('No subject specified.');
        window.history.back();
    }
    subject = subjects.find((subject) => {
        if (subject.name == subject_name) {
            return subject;
        }
    });
    // load assessment class from subject
    assessment_class = subject.assessment_classes.find((assessment_class) => {
        if (assessment_class.name == assessment_class_name) {
            return assessment_class;
        }
    });
}
// check every 5 seconds that user is online
// if user is offline, notify them
let internetChecker = setInterval(() => {
    if (!window.navigator.onLine) {
        alert('You are currently offline. Any changes you make will not be saved to our servers.');
        clearInterval(internetChecker);
    }
}, 5000);
