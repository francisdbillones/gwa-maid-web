import * as api from './api_interface.js';
import * as helpers from './helpers.js';
import * as cache from './cache.js';
// check for localStorage support
if (!helpers.verifyLocalStorage()) {
    alert('We\'re sorry, but your browser does not support local storage. Our site will not work for you.');
    window.close();
}
const token = localStorage.getItem('token');
// get parameters from arguments in URL
let url = new URL(window.location.href);
const subject_name = url.searchParams.get('subject');
const assessment_class_name = url.searchParams.get('assessment_class');
const assessment_name = url.searchParams.get('assessment');
let subjects;
let subject;
let assessment_class;
let assessment;
document.addEventListener('DOMContentLoaded', async () => {
    // wait for response from server. when response is received,
    // clear the loading indicator and display content
    const main = document.getElementById('container');
    const main_loading_indicator = document.getElementById('main-loading-indicator');
    await helpers.redirectIfNotAuthenticated(token);
    await loadVariables();
    main_loading_indicator.classList.add('d-none');
    main.classList.remove('d-none');
    const back_button = document.getElementById('back-button');
    back_button.onclick = () => {
        window.location.replace(`${helpers.BASE_URL}/htdocs/assessment_class.html?` + new URLSearchParams({
            subject: subject.name,
            assessment_class: assessment_class.name
        }));
    };
    const sign_out_button = document.getElementById('sign-out-button');
    sign_out_button.onclick = () => {
        helpers.signOut();
        window.location.replace(helpers.BASE_URL);
    };
    const grade_label = document.getElementById('grade-label');
    grade_label.innerText = `Your grade for ${assessment.name} is: ${assessment.grade.toPrecision(4)}%`;
    const form = document.querySelector('form');
    const updated_assessment_name_field = form.elements.namedItem('assessment-name');
    const updated_assessment_grade_field = form.elements.namedItem('assessment-grade');
    // pre-fill fields with existing data
    updated_assessment_name_field.value = assessment.name;
    updated_assessment_grade_field.value = assessment.grade.toPrecision(4);
    updated_assessment_name_field.oninput = () => {
        updated_assessment_name_field.setCustomValidity('');
    };
    updated_assessment_grade_field.oninput = () => {
        updated_assessment_grade_field.setCustomValidity('');
    };
    form.onsubmit = (e) => {
        e.preventDefault();
        let updated_assessment_name = updated_assessment_name_field.value;
        let updated_assessment_grade = parseFloat(updated_assessment_grade_field.value);
        // check that there were changes made. if no changes,
        // simply ignore the submission
        if (assessment.name == updated_assessment_name &&
            assessment.grade == updated_assessment_grade) {
            return;
        }
        // on submit, add loading indicator to button
        // and make it disabled
        const submit_button = document.getElementById('submit-button');
        submit_button.setAttribute('disabled', '');
        document.getElementById('submit-button-loading-indicator').classList.remove('d-none');
        // check validity for each input field
        document.querySelectorAll('input').forEach((field) => {
            if (!field.checkValidity()) {
                field.reportValidity();
            }
        });
        // check that the name is different, then validate
        if (assessment.name != updated_assessment_name) {
            // check that the name doesn't already exist
            if (assessment_class.assessments.
                map(assessment => assessment.name).
                includes(updated_assessment_name)) {
                updated_assessment_name_field.setCustomValidity('That name is taken.');
                updated_assessment_name_field.reportValidity();
                submit_button.removeAttribute('disabled');
                document.getElementById('submit-button-loading-indicator').classList.add('d-none');
                return;
            }
            else {
                updated_assessment_name_field.setCustomValidity('');
            }
        }
        // save changes to localStorage
        cache.editAssessment(subject, assessment_class, assessment, updated_assessment_name, updated_assessment_grade);
        // if online, save changes to server
        if (window.navigator.onLine) {
            api.editAssessment(token, subject, assessment_class, assessment, updated_assessment_name, updated_assessment_grade).then((response) => {
                if (response) {
                    window.location.replace(`${helpers.BASE_URL}/htdocs/assessment_class.html?` + new URLSearchParams({
                        subject: subject.name,
                        assessment_class: assessment_class.name,
                    }));
                }
                else {
                    submit_button.removeAttribute('disabled');
                    document.getElementById('submit-button-loading-indicator').classList.add('d-none');
                    alert('Something went wrong when uploading data.');
                }
            });
        }
    };
});
async function loadVariables() {
    if (window.navigator.onLine) {
        subjects = await api.getSubjects(token);
        cache.saveSubjects(subjects);
    }
    else {
        subjects = cache.getSubjects();
    }
    if (subject_name == null) {
        alert('No subject specified.');
        window.history.back();
    }
    subject = subjects.find(subject => {
        if (subject.name == subject_name) {
            return subject;
        }
    });
    // load assessment class from subject
    if (assessment_class_name == null) {
        alert('No assessment class specified.');
        window.history.back();
    }
    assessment_class = subject.assessment_classes.find(assessment_class => {
        if (assessment_class.name == assessment_class_name) {
            return assessment_class;
        }
    });
    // load assessment from assessment class
    if (assessment_name == null) {
        alert('No assessment specified');
        window.history.back();
    }
    assessment = assessment_class.assessments.find(assessment => {
        if (assessment.name == assessment_name) {
            return assessment;
        }
    });
}
// check every 5 seconds that user is online
// if user is offline, notify them
let internetChecker = setInterval(() => {
    if (!window.navigator.onLine) {
        alert('You are currently offline. You will still be able to navigate and view the content but any changes will not be saved.');
        clearInterval(internetChecker);
    }
}, 5000);
