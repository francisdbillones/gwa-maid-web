export function saveSubjects(subjects) {
    let serialized_subjects = JSON.stringify(subjects);
    localStorage.setItem('subjects', serialized_subjects);
}
export function getSubjects() {
    let subjects_from_local_storage = localStorage.getItem('subjects');
    if (subjects_from_local_storage == null) {
        return null;
    }
    let subjects = JSON.parse(subjects_from_local_storage);
    return subjects;
}
export function addSubject(subject) {
    let subjects = getSubjects();
    subjects.unshift(subject);
    saveSubjects(subjects);
}
export function addAssessmentClass(assessment_class) {
    let subjects = getSubjects();
    subjects = subjects.map(subject => {
        if (subject.name == assessment_class.subject.name) {
            subject.assessment_classes.unshift(assessment_class);
        }
        return subject;
    });
    saveSubjects(subjects);
}
export function addAssessment(subject, assessment) {
    let subjects = getSubjects();
    subjects = subjects.map(_subject => {
        if (_subject.name == subject.name) {
            _subject.assessment_classes = _subject.assessment_classes.map(assessment_class => {
                if (assessment_class.name == assessment.assessment_class.name) {
                    assessment_class.assessments.unshift(assessment);
                }
                return assessment_class;
            });
        }
        return _subject;
    });
    saveSubjects(subjects);
}
export function editAssessment(parent_subject, parent_assessment_class, assessment, updated_name, updated_grade) {
    let subjects = getSubjects();
    subjects = subjects.map(subject => {
        if (subject.name == parent_subject.name) {
            subject.assessment_classes = subject.assessment_classes.map(assessment_class => {
                if (assessment_class.name == parent_assessment_class.name) {
                    assessment_class.assessments = assessment_class.assessments.map(_assessment => {
                        if (_assessment.name == assessment.name) {
                            _assessment.name = updated_name;
                            _assessment.grade = updated_grade;
                        }
                        return _assessment;
                    });
                }
                return assessment_class;
            });
        }
        return subject;
    });
    saveSubjects(subjects);
}
