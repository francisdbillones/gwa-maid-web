import * as api from './api_interface.js';
const REPO_NAME = 'gwa-maid-web';
export const BASE_URL = `${window.location.origin}`;
export async function redirectIfNotAuthenticated(token) {
    let verified = await api.verifyToken(token);
    if (!verified) {
        window.location.replace(BASE_URL);
    }
}
export function signOut() {
    localStorage.removeItem('token');
    window.location.replace(BASE_URL);
}
export async function redirectOptionallyIfAuthenticated(token) {
    let verified = await api.verifyToken(token);
    if (verified) {
        let redirect = confirm('You\'re already logged in. Would you like to go to the home page?');
        if (redirect) {
            window.location.replace(`${BASE_URL}/htdocs/home.html`);
        }
    }
}
export function verifyLocalStorage() {
    return Storage !== void (0);
}
export function parseQuery(queryString) {
    let query = {};
    let pairs = (queryString[0] === '?' ? queryString.substr(1) : queryString).split('&');
    for (let i = 0; i < pairs.length; i++) {
        let pair = pairs[i].split('=');
        query[decodeURIComponent(pair[0])] = decodeURIComponent(pair[1] || '');
    }
    return query;
}
export function writeSubjectsToTable(table, subjects) {
    // create a new tbody
    let tbody_of_subjects = document.createElement('tbody');
    subjects.forEach((subject, index) => {
        let row = tbody_of_subjects.insertRow(index);
        let anchor = document.createElement('a');
        anchor.setAttribute('href', `${BASE_URL}/htdocs/subject.html?` + new URLSearchParams({ subject: subject.name }));
        anchor.innerHTML = subject.name;
        let predicted_grade_in_bold = document.createElement('b');
        predicted_grade_in_bold.innerText = computeGWAFromPercentGrade(subject.predicted_grade).toPrecision(4);
        row.insertCell(0).appendChild(anchor);
        row.insertCell(1).innerText = subject.last_updated;
        row.insertCell(2).innerText = subject.weight.toString() + '%';
        row.insertCell(3).appendChild(predicted_grade_in_bold);
    });
    // replace the old tbody with the new tbody
    let old_tbody_of_subjects = table.tBodies[0];
    table.replaceChild(tbody_of_subjects, old_tbody_of_subjects);
}
export function writeAssessmentClassesToTable(table, subject) {
    // create a new tbody
    let tbody_of_assessment_classes = document.createElement('tbody');
    subject.assessment_classes.forEach((assessment_class, index) => {
        let row = tbody_of_assessment_classes.insertRow(index);
        let anchor = document.createElement('a');
        anchor.setAttribute('href', `${BASE_URL}/htdocs/assessment_class.html?` +
            new URLSearchParams({
                subject: subject.name,
                assessment_class: assessment_class.name
            }));
        anchor.innerText = assessment_class.name;
        let predicted_grade_in_bold = document.createElement('b');
        predicted_grade_in_bold.innerText = assessment_class.predicted_grade.toPrecision(4) + '%';
        row.insertCell(0).appendChild(anchor);
        row.insertCell(1).innerText = assessment_class.last_updated;
        row.insertCell(2).innerText = assessment_class.weight.toString() + '%';
        row.insertCell(3).appendChild(predicted_grade_in_bold);
    });
    let old_tbody_of_assessment_classes = table.tBodies[0];
    table.replaceChild(tbody_of_assessment_classes, old_tbody_of_assessment_classes);
}
export function writeAssessmentsToTable(table, subject, assessment_class) {
    // create a new tbody
    let tbody_of_assessments = document.createElement('tbody');
    assessment_class.assessments.forEach((assessment, index) => {
        let row = tbody_of_assessments.insertRow(index);
        let anchor = document.createElement('a');
        anchor.setAttribute('href', `${BASE_URL}/htdocs/assessment.html?` +
            new URLSearchParams({
                subject: subject.name,
                assessment_class: assessment_class.name,
                assessment: assessment.name
            }));
        anchor.innerText = assessment.name;
        let grade_in_bold = document.createElement('b');
        grade_in_bold.innerText = assessment.grade.toPrecision(4) + '%';
        row.insertCell(0).appendChild(anchor);
        row.insertCell(1).innerText = assessment.last_updated;
        row.insertCell(2).appendChild(grade_in_bold);
    });
    let old_tbody_of_assessments = table.tBodies[0];
    table.replaceChild(tbody_of_assessments, old_tbody_of_assessments);
}
export function computePredictedGWAFromSubjects(subjects) {
    let total_predicted_grade = 0;
    subjects.forEach(subject => {
        let predicted_grade_in_gwa = computeGWAFromPercentGrade(subject.predicted_grade);
        total_predicted_grade += predicted_grade_in_gwa * (subject.weight / 100);
    });
    let total_weight = subjects.map(subject => subject.weight).reduce((total, weight) => total + (weight / 100), 0);
    if (total_weight < 1) {
        total_predicted_grade += (1 - total_weight) * computeGWAFromPercentGrade(80);
    }
    return total_predicted_grade;
}
export function computeGWAFromPercentGrade(percent_grade) {
    if (percent_grade > 100 || percent_grade < 0)
        return null;
    else if (percent_grade >= 96)
        return 1.0;
    else if (percent_grade >= 90)
        return 1.25;
    else if (percent_grade >= 84)
        return 1.5;
    else if (percent_grade >= 78)
        return 1.75;
    else if (percent_grade >= 72)
        return 2.0;
    else if (percent_grade >= 66)
        return 2.25;
    else if (percent_grade >= 60)
        return 2.5;
    else if (percent_grade >= 55)
        return 2.75;
    else if (percent_grade >= 50)
        return 3.0;
    else if (percent_grade >= 40)
        return 4.0;
    else
        return 5.0;
}
