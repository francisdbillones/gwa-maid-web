import * as api from './api_interface.js';
import * as helpers from './helpers.js';

// check for localStorage support
if (!helpers.verifyLocalStorage()) {
    alert(
        'We\'re sorry, but your browser does not support local storage. Our site will not work for you.'
    );
    window.close();
}

const token: string | null = localStorage.getItem('token');

document.addEventListener('DOMContentLoaded', async () => {
    const main = document.getElementById('container');
    const main_loading_indicator = document.getElementById('main-loading-indicator');

    await helpers.redirectOptionallyIfAuthenticated(token);
    
    main_loading_indicator.classList.add('d-none');
    main.classList.remove('d-none');

    const register_form: HTMLFormElement = <HTMLFormElement>(
        document.getElementById('register-form')
    );

    register_form.onsubmit = async (e) => {
        e.preventDefault();

        const username: string = (<HTMLInputElement>(
            register_form.elements.namedItem('username')
        )).value;

        const password: string = (<HTMLInputElement>(
            register_form.elements.namedItem('password')
        )).value;

        const confirm_password: string = (<HTMLInputElement>(
            register_form.elements.namedItem('confirm-password')
        )).value;

        // on submit, add loading indicator to button
        // and make it disabled
        const submit_button = document.getElementById('submit-button');

        submit_button.setAttribute('disabled', '');
        document.getElementById('submit-button-loading-indicator').classList.remove('d-none');

        if (password != confirm_password) {
            alert('Passwords do not match.');
            window.location.reload();
        } else {
            api.register(username, password).then((response_token) => {
                if (response_token != null) {
                    localStorage.setItem('token', response_token);

                    alert('Registration successful.');
                    window.location.replace(`${helpers.BASE_URL}/htdocs/home.html`);

                } else {
                    submit_button.removeAttribute('disabled');
                    document.getElementById('submit-button-loading-indicator').classList.add('d-none');

                    alert('Registration failed.');
                }
            })
        }
    };
});

// check every 5 seconds that user is online
// if user is offline, notify them
let internetChecker = setInterval(() => {
    if (!window.navigator.onLine) {
        alert('You are currently offline. Any changes you make will not be saved to our servers.');
        clearInterval(internetChecker);
    }
}, 5000)
