import * as api from './api_interface.js'
import * as helpers from './helpers.js'

const token: string | null = localStorage.getItem('token')

document.addEventListener('DOMContentLoaded', async () => {
    let verified = await api.verifyToken(token);
    
    if (verified){
        const sign_out_section = document.getElementById('sign-out-section');
        sign_out_section.classList.remove('d-none');

        document.getElementById('sign-out-section').onclick = () => {
            helpers.signOut()
        }
    } else {
        const sign_in_section = document.getElementById('sign-in-section');
        sign_in_section.classList.remove('d-none');
    }
})