import * as api from './api_interface.js'
import * as helpers from './helpers.js'
import * as cache from './cache.js'
import { Subject, AssessmentClass } from './models.js'

// check for localStorage support
if (!helpers.verifyLocalStorage()) {
    alert('We\'re sorry, but your browser does not support local storage. Our site will not work for you.')
    window.close()
}

const token: string | null = localStorage.getItem('token')

let subjects: Subject[];
let subject: Subject;

document.addEventListener('DOMContentLoaded', async () => {
    const main = document.getElementById('container');
    const main_loading_indicator = document.getElementById('main-loading-indicator');

    await helpers.redirectIfNotAuthenticated(token);
    await loadVariables();

    main_loading_indicator.classList.add('d-none');
    main.classList.remove('d-none');

    const sign_out_button = document.getElementById('sign-out-button')
    sign_out_button.onclick = () => {
        helpers.signOut();
        window.location.replace(helpers.BASE_URL)
    }

    const predicted_grade_label = document.getElementById('predicted-grade-label');

    let predicted_grade_in_gwa = helpers.computeGWAFromPercentGrade(subject.predicted_grade)

    predicted_grade_label.innerText = `Your predicted grade for ${subject.name}: ${predicted_grade_in_gwa.toPrecision(4)}`

    const back_button = document.getElementById('back-button');
    back_button.onclick = () => {
        window.location.replace(`${helpers.BASE_URL}/htdocs/home.html`);
    }

    const table: HTMLTableElement = document.querySelector('table');

    const reload: HTMLButtonElement = <HTMLButtonElement>document.getElementById('reload');

    const form: HTMLFormElement = <HTMLFormElement>document.querySelector('form');

    const assessment_class_name_field = <HTMLInputElement>form.elements.namedItem('assessment-class-name');
    const assessment_class_weight_field = <HTMLInputElement>form.elements.namedItem('assessment-class-weight');

    assessment_class_name_field.oninput = () => {
        assessment_class_name_field.setCustomValidity('');
    }

    assessment_class_weight_field.oninput = () => {
        assessment_class_weight_field.setCustomValidity('');
    }

    if (subject.assessment_classes != null) {
        helpers.writeAssessmentClassesToTable(table, subject);
    }

    reload.onclick = async (e) => {
        e.preventDefault();

        const loading_indicator = reload.querySelector('span');
        loading_indicator.classList.remove('d-none');
        const reload_icon = reload.querySelector('img');
        reload_icon.classList.add('d-none')

        loadVariables().then(() => {
            loading_indicator.classList.add('d-none');
            reload_icon.classList.remove('d-none');
        });
    }

    form.onsubmit = (e) => {
        e.preventDefault();

        // check validity for each input field
        document.querySelectorAll('input').forEach((field) => {
            if (!field.checkValidity()) {
                field.reportValidity();
            }
        });

        let assessment_class_name = assessment_class_name_field.value;

        // on submit, add loading indicator to button
        // and make it disabled
        const submit_button = document.getElementById('submit-button');

        submit_button.setAttribute('disabled', '');
        document.getElementById('submit-button-loading-indicator').classList.remove('d-none');

        // check that the name doesn't already exist
        if (subject.assessment_classes.
            map(assessment_class => assessment_class.name).
            includes(assessment_class_name)) {
            assessment_class_name_field.setCustomValidity('That name is taken.');
            assessment_class_name_field.reportValidity();

            submit_button.removeAttribute('disabled');
            document.getElementById('submit-button-loading-indicator').classList.add('d-none');

            return;
        } else {
            assessment_class_name_field.setCustomValidity('');
        }

        let assessment_class_weight = parseFloat(assessment_class_weight_field.value);

        let sum_of_weights = subject.assessment_classes.map(assessment_class => assessment_class.weight).reduce((sum, weight) => sum + weight, 0);

        // check that the total weight won't go over 100%
        if ((sum_of_weights + assessment_class_weight) / 100 > 1) {
            let total_weight = (sum_of_weights + assessment_class_weight)
            let total_weight_in_percent = total_weight.toPrecision(4) + '%'

            assessment_class_weight_field.setCustomValidity(`Adding this weight would result in a cumulative weight of ${total_weight_in_percent}.`);
            assessment_class_weight_field.reportValidity();

            submit_button.removeAttribute('disabled');
            document.getElementById('submit-button-loading-indicator').classList.add('d-none');

            return;
        } else {
            assessment_class_weight_field.setCustomValidity('');
        }

        let assessment_class: AssessmentClass = {
            name: assessment_class_name,
            subject: subject,
            weight: assessment_class_weight,
            last_updated: new Date().toLocaleString('en-US'),
            predicted_grade: api.DEFAULT_GRADE
        };

        // save changes to localStorage
        cache.addAssessmentClass(assessment_class);

        // if online, save changes to server
        if (window.navigator.onLine) {
            api.addAssessmentClass(token, assessment_class).then((response) => {
                if (response) {
                    window.location.reload();
                } else {
                    submit_button.removeAttribute('disabled')
                    document.getElementById('submit-button-loading-indicator').classList.add('d-none');

                    alert('Something went wrong.')
                }
            })
        }
    }
})

async function loadVariables(): Promise<void> {
    if (window.navigator.onLine) {
        subjects = await api.getSubjects(token);
        cache.saveSubjects(subjects);
    } else {
        subjects = cache.getSubjects();
    }

    let subject_name = new URL(window.location.href).searchParams.get('subject');

    if (subject_name == null) {
        alert('No subject specified.');
        window.close();
    }
    subject = subjects.find((subject) => {
        if (subject.name == subject_name) {
            return subject;
        }
    });
}

// check every 5 seconds that user is online
// if user is offline, notify them
let internetChecker = setInterval(() => {
    if (!window.navigator.onLine) {
        alert('You are currently offline. Any changes you make will not be saved to our servers.');
        clearInterval(internetChecker);
    }
}, 5000)